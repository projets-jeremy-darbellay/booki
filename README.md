![Bannière de booki](./public/images/booki-banner.png)
# Site de Booki - Bases HTML5/CSS3

Dans ce projet le but était d'intégrer une maquette avec les technologies HTML5 et CSS3, sans utiliser ni framework, ni générateur de site. L'intérêt était ici de créer sa première page web.

# Sommaire
+ [Site de Booki - Bases HTML5/CSS3](#site-de-booki-bases-html5css3)
+ [Sommaire](#sommaire)
+ [Installation et lancement](#installation-et-lancement)
+ [Démonstration](#démonstration)
+ [Technologies utilisées](#technologies-utilisées)
+ [License](#license)

# Installation et lancement

Aucune installation, c'est une page html stylisée avec CSS3, visitez la démo !

# Démonstration

Vous retrouverez la démonstration du site à cette adresse : [site de Booki](https://projets-jeremy-darbellay.gitlab.io/booki).
Dans le code, les fichiers de démonstrations sont dans le dossier "public", comme il n'y a pas de back-end sur l'hébergement, toutes les fonctionnalités de communcation ont été désactivées

# Technologies utilisées

Pour ce projet, je n'ai utilisé que HTML5 et CSS3, aucun framework ni aucune librairie n'a été ajoutée.

# License

[MIT](https://gitlab.com/projets-jeremy-darbellay/booki/-/blob/master/LICENSE.md)
